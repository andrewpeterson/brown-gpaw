Installation instructions
=========================

NOTE: We are still having issues since the new RHEL upgrade at the CCV.
These instructions produce a mostly workable version, but we sometimes have jobs hang on multiple nodes.
Single-node jobs seem to work.
CCV is working to fix this issue.

Pre-installed Peterson group version
------------------------------------

#FIXME Note we don't yet have a pre-installed version as of 2024-02-12; making sure individual installations are stable first.  Please skip to 'Custom installation'.

If you have read-access to the Peterson group's shared software directory (/gpfs/data/ap31/software), then you should be able to run a pre-installed version of GPAW 22.8.0 by adding the line below to your `.modules` file:

```bash
# Group software installations.
source /gpfs/data/ap31/software/2023/source_me
```

Now (after logging back in again), you can load GPAW 22.8.0 with `pgroup-load-gpaw-22.08.0`.
You can submit GPAW jobs with `gpaw-submit` and `gpaw-debug-submit`.
(You don't need to follow any further instructions below.)

There is also a version from April, 2023, that contains a patch to remove noise from SJM energy calculations.
Load it with `pgroup-load-gpaw-sjm-patch-2023`.

Custom installation
-------------------

If you'd like to install your own version of GPAW 24.1.0 or the latest development version; for example to use or test new features, or better to have your own copy to hack on, these instructions should work.
You should also have a look at the official instructions on the [GPAW website](https://wiki.fysik.dtu.dk/gpaw/devel/developer_installation.html).

Here, we'll create everything in a standalone environment, such that when you want to use gpaw later you can do so by calling a command like `loadgpaw241`.
This approach shouldn't put hidden files all over your system, so you should be able to un-install later, if you choose, by just removing the directory with all of the GPAW files.
Let's first create that directory; you can place it wherever you choose by modifying `$INSTALLPATH`:

```bash
INSTALLPATH=$HOME/installs/gpaw-24.1.0  # Feel free to re-name; must be absolute path.
mkdir -p $INSTALLPATH
cd $INSTALLPATH
```

Next load all the modules that we'll need for this installation:

```bash
module load libxc/5.2.3-ncc5ir4 intel-oneapi-mkl/2023.1.0-xbcd2g3 openmpi/4.1.4s-smqniuf python/3.11.0s-ixrhc3q
```

Create a virtual environment that contains all the python dependencies.
We'll re-load this virtual environment every time we want to run a GPAW job with this version.
Create the virtual environment:


```bash
python3 -m venv gpaw-venv
source gpaw-venv/bin/activate
```

Install the python dependencies. This will take some time.

```bash
python3 -m pip install --upgrade pip
python3 -m pip install numpy
python3 -m pip install scipy
python3 -m pip install matplotlib
# Below are optional, mostly for those doing development.
python3 -m pip install ipython  # for interactive python shell
python3 -m pip install flake8  # for code style / bug checking
python3 -m pip install pytest  # for unit tests
python3 -m pip install pytest-xdist  # for unit tests
python3 -m pip install sphinx  # for documentation building
python3 -m pip install sphinx-rtd-theme  # for documentation building
```

Download and build ASE; here we'll make an "editable" version in case you want to hack on it.
Note you should remove the "-b 3.22.1" if you want the latest trunk version; if you want your own fork of ASE, use a line like the commented-out one.

```bash
ASEPATH=$INSTALLPATH/source/ase
mkdir -p $ASEPATH
cd $ASEPATH
git clone -b 3.22.1 https://gitlab.com/ase/ase.git  # for version 3.22.1
#git clone git@gitlab.com:andrew_peterson/ase.git  # E.g., your own development version
cd ase
python3 -m pip install --editable .
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $ASEPATH/ase/ase/cli/complete.py" ase
```

We've stored the configuration files and submission scripts that are unique to Brown in the "brown-gpaw" repository.
(Presumably, you are reading this file from within this repository!)
Clone this so you can get these files, using one of the two modes below.

```bash
cd $INSTALLPATH
git clone https://bitbucket.org/andrewpeterson/brown-gpaw.git
#git clone git@bitbucket.org:andrewpeterson/brown-gpaw.git
```

Download GPAW into a local folder, which we'll call `source`.
Choose one of the methods below, depending on if you want the latest development version, the exact stable 24.1.0 version, or your own development version.
Also copy our own version of `siteconfig.py` to this directory.
*Note:* If you are planning on working on a merge request, make sure to clone your own version of gpaw!
E.g., `git clone git@gitlab.com:andrew_peterson/gpaw.git`, and make sure your own repository is correctly updated to the version you want.

```bash
cd source
git clone -b 24.1.0 https://gitlab.com/gpaw/gpaw.git  # Exact 24.1.0 version
#git clone https://gitlab.com/gpaw/gpaw.git  # Latest development version
#git clone git@gitlab.com:andrew_peterson/gpaw.git  # E.g., your own development version
cd gpaw
cp ../../brown-gpaw/24.1.0/siteconfig.py .
```

Cross your fingers, and install with

```bash
python3 -m pip install --editable .
```

(If you are developing GPAW: The `--editable` flag means that the installation will point to the git source files, so you don't have to re-install every time you change something in python. If you are installing a permanent version, you can leave this flag off. It doesn't really matter in that case.)

Check that the installation exists, install the PAW setups, and test:

```bash
cd $INSTALLPATH
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $INSTALLPATH/source/gpaw/gpaw/cli/complete.py" gpaw
gpaw info  # Checks where stuff is installed.
gpaw install-data .  # Installs the setups. Probably say 'n' to register the path.
export GPAW_SETUP_PATH=$INSTALLPATH/gpaw-setups-24.1.0  # skip this if you said 'y'
gpaw info  # Make sure the new setup directory shows up.
gpaw test
gpaw -P 4 test
```

If you are doing something fancy, you may want to run the complete test suite, which will take some hours:

```bash
cd source/gpaw
pytest -v
# Or better, submit it as a job to the queue as:
cp ../../brown-gpaw/24.1.0/run-gpaw-tests.py .
sbatch run-gpaw-tests.py
```

If all looks good, you now have a functional copy.
Now you should make a command to load it whenever you want to run a job.
Add the following to your `.bashrc` (making sure your ASE path is right):

```bash
loadgpaw(){
INSTALLPATH=$HOME/installs/gpaw-24.1.0  # Update if necessary.
ASEPATH=$INSTALLPATH/source/ase
module load libxc/5.2.3-ncc5ir4 intel-oneapi-mkl/2023.1.0-xbcd2g3 openmpi/4.1.4s-smqniuf python/3.11.0s-ixrhc3q
source $INSTALLPATH/gpaw-venv/bin/activate
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $ASEPATH/ase/ase/cli/complete.py" ase
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $INSTALLPATH/source/gpaw/gpaw/cli/complete.py" gpaw
# Comment out the next line if you chose to register your setups in ~/gpaw/rc.py.
export GPAW_SETUP_PATH=$INSTALLPATH/gpaw-setups-24.1.0
}
```

You will also need submit files that you use to submit your python scripts.
You can copy them into your bin directory as

```bash
cp $INSTALLPATH/brown-gpaw/24.1.0/gpaw-debug-submit $INSTALLPATH/gpaw-venv/bin/
cp $INSTALLPATH/brown-gpaw/24.1.0/gpaw-submit $INSTALLPATH/gpaw-venv/bin/
```

Then you should be able to submit jobs as normal, like

```bash
loadgpaw # you only need to run this once per session, or put it in your bashrc.
#gpaw-debug-submit -c 4 -t 0:15:00 run.py  # debug job
gpaw-submit -n 1 -c 24 -t 50:00:00 run.py
```

CCV Installation Notes
======================

For archival purposes, the file CCVREADME is one created by the CCV which contains the installation procedure that the CCV used in installing this.
Note that this file is contained in the 21.1.0 folder, as this version uses the identical CCV install files.

AGTS: Big Tests and Documentation Figures
=========================================

(Don't worry about this section unless you know you need AGTS! Most people don't.)

GPAW uses the "Advanced GPAW Testing System" (AGTS) to run especially long tests and to make figures for the documentation that are compute-intensive.
This is basically a queueing / job-management system through `myqueue`, a side package of GPAW.
So you will need to first install myqueue and get it configured for the CCV.

Here we will assume you have done all the steps above and have created a `loadgpawdeveloper` command.
First, load this (to activate your virtual environment), then install `myqueue` with pip:

```bash
loadgpawdeveloper
python3 -m pip install myqueue
```

Next you will need a `config.py` file for our system at Brown.
Note the example we have in this repository assumes you are a member of the `ap31` group; if not, change this line as appropriate.
(You can also delete this line and will just submit to the default queue.)

```bash
cp $GPAWPATH/brown-gpaw/21.6.0/config.py ~/.myqueue/
# edit the file above as appropriate!
```

*Hack:* Myqueue does not seem to inherit the system environment in the same manner as normal job submissions does, and we did not figure out an elegant way to pass the system environment variables through.
So the workaround is to temporarily add `loadgpawdeveloper` to your `.bashrc` file; this will make sure that GPAW is always accessible to all logged-in sessions.
You only need this while the AGTS jobs are running, you can remove it later.
Do this, and you can submit the whole test suite, for example, by

```bash
cd $GPAWPATH/source/gpaw
mq workflow -p agts.py .
mq list  # see what's happening
```


Next: note crashed due to undefined variable zherk.
undefined symbol: zherk_ 
I think I need BLAS which I'll get from scalapack.
It looks to me like I'll need gcc-11.3.1 for this based on looking at siteconfig.py
netlib-scalapack-mpi/2.2.0-xugjatm might also work?
