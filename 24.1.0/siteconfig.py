"""User provided customizations.

Here one changes the default arguments for compiling _gpaw.so.

Here are all the lists that can be modified:

* libraries
  List of libraries to link: -l<lib1> -l<lib2> ...
* library_dirs
  Library search directories: -L<dir1> -L<dir2> ...
* include_dirs
  Header search directories: -I<dir1> -I<dir2> ...
* extra_link_args
  Arguments forwarded directly to linker
* extra_compile_args
  Arguments forwarded directly to compiler
* runtime_library_dirs
  Runtime library search directories: -Wl,-rpath=<dir1> -Wl,-rpath=<dir2> ...
* extra_objects
* define_macros

The following lists work like above, but are only linked when compiling
the parallel interpreter:

* mpi_libraries
* mpi_library_dirs
* mpi_include_dirs
* mpi_runtime_library_dirs
* mpi_define_macros

To override use the form:

    libraries = ['somelib', 'otherlib']

To append use the form

    libraries += ['somelib', 'otherlib']
"""

# flake8: noqa

mpi = True
compiler = 'mpicc'

###### CCV ######
# LIBRARIES 
libraries = ['xc', 'mkl_intel_lp64', 'mkl_sequential', 'mkl_core',
             'mkl_lapack95_lp64', 'mkl_blacs_intelmpi_lp64',
             'mkl_avx512', 'mkl_def', 'mkl_scalapack_lp64',
             'pthread']

library_dirs += ['/oscar/rt/9.2/software/0.20-generic/0.20.1/opt/spack/linux-rhel9-x86_64_v3/gcc-11.3.1/intel-oneapi-mkl-2023.1.0-xbcd2g3myicmoznxp4pajepy7y2xzsvh/lib/intel64']
library_dirs += ['/oscar/rt/9.2/software/0.20-generic/0.20.1/opt/spack/linux-rhel9-x86_64_v3/gcc-11.3.1/intel-oneapi-mkl-2023.1.0-xbcd2g3myicmoznxp4pajepy7y2xzsvh/mkl/2023.1.0/lib/intel64']

include_dirs += ['/oscar/rt/9.2/software/0.20-generic/0.20.1/opt/spack/linux-rhel9-x86_64_v3/gcc-11.3.1/intel-oneapi-mkl-2023.1.0-xbcd2g3myicmoznxp4pajepy7y2xzsvh/include']
include_dirs += ['/oscar/rt/9.2/software/0.20-generic/0.20.1/opt/spack/linux-rhel9-x86_64_v3/gcc-11.3.1/intel-oneapi-mkl-2023.1.0-xbcd2g3myicmoznxp4pajepy7y2xzsvh/mkl/2023.1.0/include']

# COMPILE OPTIONS
###### CCV ######

# FFTW3:
#fftw = False			# CCV
fftw = True			# CCV
if fftw:
    libraries += ['fftw3']
    library_dirs += ['/oscar/rt/9.2/software/external/gpaw_tests/gpaw-openmpi/gpaw.venv/deps/build/lib']
    include_dirs += ['/oscar/rt/9.2/software/external/gpaw_tests/gpaw-openmpi/gpaw.venv/deps/build/include']

# ScaLAPACK (version 2.0.1+ required):
scalapack = True		# CCV
if scalapack:
    libraries += ['scalapack'] 	# CCV
    library_dirs += ['/oscar/rt/9.2/software/external/gpaw_tests/gpaw-openmpi/gpaw.venv/deps/scalapack/build/lib']
    library_dirs += ['/oscar/rt/9.2/software/external/gpaw_tests/gpaw-openmpi/gpaw.venv/deps/scalapack/build']
    #library_dirs += ['/oscar/rt/9.2/software/0.20-generic/0.20.1/opt/spack/linux-rhel9-x86_64_v3/gcc-11.3.1/netlib-scalapack-2.2.0-xugjatmo5czzasxawkdx43zfuk3t7omq/lib']

# Use Elpa (requires ScaLAPACK and Elpa API 20171201):

# LibXC:
# In order to link libxc installed in a non-standard location
# (e.g.: configure --prefix=/home/user/libxc-2.0.1-1), use:

# - dynamic linking (requires rpath or setting LD_LIBRARY_PATH at runtime):
libxc = True
if libxc:
    xc = '/oscar/rt/9.2/software/0.20-generic/0.20.1/opt/spack/linux-rhel9-x86_64_v3/gcc-11.3.1/libxc-5.2.3-ncc5ir4olbc6qjmncc67ihj7m6icgmvz'
    include_dirs += [xc + 'include']
    library_dirs += [xc + 'lib']
    # You can use rpath to avoid changing LD_LIBRARY_PATH:
    extra_link_args += ['-Wl,-rpath={xc}/lib'.format(xc=xc)]
    if 'xc' not in libraries:
        libraries.append('xc')


