Here we are updating to the build style of Prithvi.
Note that we have had many issues with openMPI across multiple nodes that we have not fully resolved; it's safest to run on a single node if you can get away with it.

Creating your own version of GPAW
---------------------------------

Ideally, you should be loading no modules from inside your .bashrc or .modules file, so that you are starting with a clean environment. You can check if you have any modules loaded with `modules list`; it will ideally say "No modules loaded".

Now, load the modules we'll need for GPAW:

```bash
module load libxc/5.2.3-ncc5ir4
module load intel-oneapi-mkl/2023.1.0-xbcd2g3
module load openmpi/4.1.4s-smqniuf
module load python/3.11.0s-ixrhc3q
```

Next, choose a directory for the installation.
Edit the first line to match where you'd like your intall to go.

```bash
INSTALLPATH=$HOME/installs/gpaw24.6.0  # edit me if needed
mkdir -p $INSTALLPATH
cd $INSTALLPATH
```

We will create a python virtual environment to hold this installation, so it does not compete with any other python installations.
Create the virtual environment and install the necessary modules:

```bash
python3 -m venv gpaw-venv
source gpaw-venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install numpy
python3 -m pip install scipy
python3 -m pip install matplotlib
# Below are optional, mostly for those doing development.
python3 -m pip install ipython  # for interactive python shell
python3 -m pip install flake8  # for code style / bug checking
python3 -m pip install pytest  # for unit tests
python3 -m pip install pytest-xdist  # for unit tests
python3 -m pip install sphinx  # for documentation building
python3 -m pip install sphinx-rtd-theme  # for documentation building
```

Next, install ASE.
Here we're assuming you want your own editable version of ASE that you can hack.
If you don't want this, you should be able to skip the next step and GPAW will just pip install the correct version of ASE for you.

```bash
ASEPATH=$INSTALLPATH/source/ase
mkdir -p $ASEPATH
cd $ASEPATH
git clone -b 3.23.0 https://gitlab.com/ase/ase.git  # for version 3.23.0
#git clone git@gitlab.com:andrew_peterson/ase.git  # E.g., your own development version
cd ase/
python3 -m pip install --editable .
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $ASEPATH/ase/ase/cli/complete.py" ase
```

You might want to test your install of ASE now (e.g., make sure `which ase` points to this installation, and make sure that `ase gui` opened).

We keep the necessary source files for our installation in a repository called 'brown-gpaw', which is presumably where you are reading this from now.
Clone this repository:

```bash
cd $INSTALLPATH/source
git clone https://bitbucket.org/andrewpeterson/brown-gpaw.git
#git clone git@bitbucket.org:andrewpeterson/brown-gpaw.git  # if you have your own
```

Now download GPAW and add the siteconfig you just downloaded to the GPAW folder:

```bash
cd $INSTALLPATH/source
git clone -b 24.6.0 https://gitlab.com/gpaw/gpaw.git  # Exact 24.6.0 version
#git clone https://gitlab.com/gpaw/gpaw.git  # Latest development version
#git clone git@gitlab.com:andrew_peterson/gpaw.git  # E.g., your own development version
cd gpaw
cp ../brown-gpaw/24.6.0/siteconfig.py .
```

Cross your fingers, and install GPAW with:

```bash
python3 -m pip install --editable .
```

Check that the installation exists, install the PAW setups, and test:

```bash
cd $INSTALLPATH
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $INSTALLPATH/source/gpaw/gpaw/cli/complete.py" gpaw
gpaw info  # Checks where stuff is installed.
gpaw install-data .  # Installs the setups. Probably say 'n' to register the path.
export GPAW_SETUP_PATH=$INSTALLPATH/gpaw-setups-24.1.0  # skip this if you said 'y'
gpaw info  # Make sure the new setup directory shows up.
gpaw test
gpaw -P 4 test
```

If you are doing something fancy, you may want to run the complete test suite, which will take some h    ours:

```bash
cd source/gpaw
pytest -v
# Or better, submit it as a job to the queue as:
cp ../../brown-gpaw/24.1.0/run-gpaw-tests.py .
sbatch run-gpaw-tests.py
```

Now we'll need to make a file that you can source any time you want to load GPAW.
We'll just create a local file called SOURCEME.sh for this purpose:

```bash
cd $INSTALLPATH
gedit SOURCEME.sh
```

Paste the following lines into SOURCEME.sh, save, and close the file.
You may need to edit this if you did something different than the instructions above.

```bash
INSTALLPATH=$HOME/installs/gpaw24.6.0  # edit me if needed
ASEPATH=$INSTALLPATH/source/ase
module load libxc/5.2.3-ncc5ir4
module load intel-oneapi-mkl/2023.1.0-xbcd2g3
module load openmpi/4.1.4s-smqniuf
module load python/3.11.0s-ixrhc3q
source $INSTALLPATH/gpaw-venv/bin/activate
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $ASEPATH/ase/ase/cli/complete.py" ase
complete -o default -C "$INSTALLPATH/gpaw-venv/bin/python3 $INSTALLPATH/source/gpaw/gpaw/cli/complete.py" gpaw
export GPAW_SETUP_PATH=$INSTALLPATH/gpaw-setups-24.1.0  # skip this if you said 'y'
```

Now, whenever you want to use this version of GPAW, just `cd` to this directory and type `source SOURCEME.sh`.
You should see the (gpaw-venv) at your prompt, which shows you have loaded the virtual environment.
You may want to add a shortcut to your .bashrc to make this faster, e.g., `gedit ~/.bashrc` and add the lines:

```
loadgpaw24.6.0(){
source $HOME/installs/gpaw24.6.0/SOURCME.sh
}
```

You'll also probably want a copy of `gpaw-submit`, which you'll get as an executable if you copy it into your virtual environment:

```bash
cd $INSTALLPATH
cp source/brown-gpaw/24.6.0/gpaw-submit gpaw-venv/bin/
```
